## Prometheus

This role:
  * installs and configure [Prometheus](https://prometheus.io)
  * configures rules and targets

## Role parameters

| name                            | value     | optionnal | default value     | description                               |
| --------------------------------|-----------|-----------|-------------------|-------------------------------------------|
| prometheus_configuration_file   | string    | yes       |                   | template file for configuration file      |
| prometheus_rules_files          | array     | yes       | []                | rules files                               |
| prometheus_targets_files        | array     | yes       | []                | targets files                             |



## Using this role

### ansible galaxy
Add the following in your *requirements.yml*.
```yaml
---
- src: https://gitlab.com/myelefant1/ansible-roles3/prometheus.git
  scm: git
```

### Sample playbook

```yaml
- hosts: all
  roles:
    - role: prometheus
      prometheus_configuration_file: tests/files/prometheus.yml
      prometheus_rules_files: ["tests/files/rules.d/rules1.yml"]
      prometheus_targets_files:
        - "tests/files/targets/targets1.yml"
        - "tests/files/targets/targets2.yml"
}
```

## Tests

You could launch tests with:
```bash
tests/bash_unit tests/tests*
```
