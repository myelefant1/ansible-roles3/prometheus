#!/usr/bin/env bash_unit

# You must specify an image on wich to run your tests using RU_ENV_IMAGE variable.
# It is done either here :
RU_ENV_IMAGE=debian10_cached
RU_ENV_NAME=prometheus

# RU_ENV_IMAGE=centos7
# Or as environment variable in the shell launching the tests

# FOR MORE DOCUMENTATION, READ THE tests_tuto FILE IN THE tests DIRECTORY

. lib/role_unit

test_idempotency() {
  assert "$(ru_run_ansible_command) | grep 'changed=' | grep -v 'changed=[^0]'"
}

test_prometheus_is_started() {
  assert "ru_run curl http://localhost:9090"
}

test_prometheus_config_is_set() {
  assert "ru_run [ -f /etc/prometheus/prometheus.yml ]"
  assert "ru_run grep -E 'testLabel:.*it_is_working' /etc/prometheus/prometheus.yml"
}

test_prometheus_rules_are_present() {
  assert_equals 755 $(ru_run stat --format "%a" /etc/prometheus/rules)
  assert "ru_run [ -f /etc/prometheus/rules/rules1.yml ]"
}

test_target_files_are_present() {
  assert "ru_run [ -f /etc/prometheus/file_sd/targets1.yml ]"
  assert "ru_run [ -f /etc/prometheus/file_sd/targets2.yml ]"
}

setup_suite() {
  ru_init
  cat > ${ru_ansible_playbook} <<EOF
- hosts: all
  roles:
    - role: prometheus
      prometheus_configuration_file: tests/files/prometheus.yml
      prometheus_rules_files: ["tests/files/rules.d/rules1.yml"]
      prometheus_targets_files:
        - "tests/files/targets/targets1.yml"
        - "tests/files/targets/targets2.yml"
EOF
  ru_run apt-get install --yes procps
  ru_run_ansible verbose
}

# vim: syntax=sh
